#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/class-a-end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/node-container.h"
#include "ns3/position-allocator.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/lora-radio-energy-model-helper.h"
#include "ns3/file-helper.h"
#include "ns3/names.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/csma-helper.h"
#include "ns3/csma-channel.h"
#include "ns3/csma-net-device.h"

#include "ns3/point-to-point-module.h"

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-module.h"

#include <algorithm>
#include <ctime>

//#ifndef Forwardercdma_H
//#define Forwardercdma_H

#include "ns3/application.h"
#include "ns3/lora-net-device.h"
#include "ns3/point-to-point-net-device.h"
#include "ns3/nstime.h"
#include "ns3/attribute.h"

namespace ns3 {
namespace lorawan {

class Forwardercdma : public Application
{
public:
  Forwardercdma ();
  ~Forwardercdma ();

  static TypeId GetTypeId (void);
  void SetLoraNetDevice (Ptr<LoraNetDevice> loraNetDevice);
  void SetCsmaNetDevice (Ptr<CsmaNetDevice> CsmaNetDevice);
  bool ReceiveFromLora (Ptr<NetDevice> loraNetDevice, Ptr<const Packet> packet,
                        uint16_t protocol, const Address& sender);
  bool ReceiveFromCsmaNetDevice (Ptr<NetDevice> CsmaNetDevice,
                                Ptr<const Packet> packet, uint16_t protocol,
                                const Address& sender);
  void StartApplication (void);
  void StopApplication (void);

private:
  Ptr<LoraNetDevice> m_loraNetDevice; //!< Pointer to the node's LoraNetDevice
  Ptr<CsmaNetDevice> m_CsmaNetDevice; //!< Pointer to the

};

class ForwardercdmaHelper
{
public:
  ForwardercdmaHelper ();

  ~ForwardercdmaHelper ();

  void SetAttribute (std::string name, const AttributeValue &value);

  ApplicationContainer Install (NodeContainer c) const;

  ApplicationContainer Install (Ptr<Node> node) const;

private:
  Ptr<Application> InstallPriv (Ptr<Node> node) const;

  ObjectFactory m_factory;
};
ForwardercdmaHelper::ForwardercdmaHelper ()
{
  m_factory.SetTypeId ("ns3::Forwardercdma");
}

ForwardercdmaHelper::~ForwardercdmaHelper ()
{
}

void
ForwardercdmaHelper::SetAttribute (std::string name, const AttributeValue &value)
{
  m_factory.Set (name, value);
}

ApplicationContainer
ForwardercdmaHelper::Install (Ptr<Node> node) const
{
  return ApplicationContainer (InstallPriv (node));
}

ApplicationContainer
ForwardercdmaHelper::Install (NodeContainer c) const
{
  ApplicationContainer apps;
  for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
      apps.Add (InstallPriv (*i));
    }

  return apps;
}

Ptr<Application>
ForwardercdmaHelper::InstallPriv (Ptr<Node> node) const
{
  //NS_LOG_FUNCTION (this << node);

  Ptr<Forwardercdma> app = m_factory.Create<Forwardercdma> ();

  app->SetNode (node);
  node->AddApplication (app);

  // Link the Forwardercdma to the NetDevices
  for (uint32_t i = 0; i < node->GetNDevices (); i++)
    {
      Ptr<NetDevice> currentNetDevice = node->GetDevice (i);
      if (currentNetDevice->GetObject<LoraNetDevice> () != 0)
        {
          Ptr<LoraNetDevice> loraNetDevice =
            currentNetDevice->GetObject<LoraNetDevice> ();
          app->SetLoraNetDevice (loraNetDevice);
          loraNetDevice->SetReceiveCallback (MakeCallback
                                               (&Forwardercdma::ReceiveFromLora, app));
        }
      else if (currentNetDevice->GetObject<CsmaNetDevice> () != 0)
        {
          Ptr<CsmaNetDevice> csmaNetDevice =
            currentNetDevice->GetObject<CsmaNetDevice> ();

          app->SetCsmaNetDevice (csmaNetDevice);

          csmaNetDevice->SetReceiveCallback (MakeCallback
                                                       (&Forwardercdma::ReceiveFromCsmaNetDevice,
                                                       app));
        }
      else
        {
          return app;
          //NS_LOG_ERROR ("Potential error: NetDevice is neither Lora nor PointToPoint");
        }
    }

  return app;
}

NS_OBJECT_ENSURE_REGISTERED (Forwardercdma);

TypeId
Forwardercdma::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Forwardercdma")
    .SetParent<Application> ()
    .AddConstructor<Forwardercdma> ()
    .SetGroupName ("lorawan");
  return tid;
}

Forwardercdma::Forwardercdma ()
{
  //NS_LOG_FUNCTION_NOARGS ();
}

Forwardercdma::~Forwardercdma ()
{
  //NS_LOG_FUNCTION_NOARGS ();
}

void
Forwardercdma::SetCsmaNetDevice (Ptr<CsmaNetDevice>
                                     CsmaNetDevice)
{
  //NS_LOG_FUNCTION (this << CsmaNetDevice);

  m_CsmaNetDevice = CsmaNetDevice;
}

void
Forwardercdma::SetLoraNetDevice (Ptr<LoraNetDevice> loraNetDevice)
{
  //NS_LOG_FUNCTION (this << loraNetDevice);

  m_loraNetDevice = loraNetDevice;
}

bool
Forwardercdma::ReceiveFromLora (Ptr<NetDevice> loraNetDevice, Ptr<const Packet>
                            packet, uint16_t protocol, const Address& sender)
{
  //NS_LOG_FUNCTION (this << packet << protocol << sender);

  Ptr<Packet> packetCopy = packet->Copy ();

  m_CsmaNetDevice->Send (packetCopy,
                                 m_CsmaNetDevice->GetBroadcast (),
                                 0x800);

  return true;
}

bool
Forwardercdma::ReceiveFromCsmaNetDevice (Ptr<NetDevice> CsmaNetDevice,
                                    Ptr<const Packet> packet, uint16_t protocol,
                                    const Address& sender)
{
  //NS_LOG_FUNCTION (this << packet << protocol << sender);

  //Ptr<Packet> packetCopy = packet->Copy ();

  //m_loraNetDevice->Send (packetCopy);

  return true;
}

void
Forwardercdma::StartApplication (void)
{
  //NS_LOG_FUNCTION (this);

  // TODO Make sure we are connected to both needed devices
}

void
Forwardercdma::StopApplication (void)
{
  //NS_LOG_FUNCTION_NOARGS ();

  // TODO Get rid of callbacks
}

} //lorawan

} //ns3
//#endif /* Forwardercdma */


using namespace ns3;
using namespace lorawan;

// this variables can be modified in the command line e.g --radio=20.0
double radio = 20.0;
int numnodes = 1;

MobilityHelper createscenario(double radio); //random position around starcenter
MobilityHelper createstarcenter();
void printposition(NodeContainer n);
CommandLine setupcmd();

int main (int argc, char *argv[]){

    CommandLine cmd =setupcmd();
    cmd.Parse (argc, argv);
    GlobalValue::Bind("SimulatorImplementationType",
                  StringValue("ns3::RealtimeSimulatorImpl"));
    //LogComponentEnable ("LoraFrameHeader", LOG_LEVEL_ALL);
    //LogComponentEnable ("LorawanMacHeader", LOG_LEVEL_ALL);
    //LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_ALL);
    //LogComponentEnable ("Forwardercdma", LOG_LEVEL_ALL);

    LogComponentEnable ("TapBridge", LOG_LEVEL_INFO);

    NodeContainer nodes;
    nodes.Create(numnodes);
    MobilityHelper scenario = createscenario(radio);
    scenario.Install (nodes);
    //put the hub in 0.0 0.0 0.0  center


    NodeContainer hub;
    hub.Create(1);
    MobilityHelper hubposition = createstarcenter();
    hubposition.Install (hub);



    TapBridgeHelper tapBridge;
    tapBridge.SetAttribute ("Mode", StringValue ("ConfigureLocal"));
    tapBridge.SetAttribute ("DeviceName", StringValue ("virtualtwin"));
    // Agregar el dispositivo al modelo de red de NS-3




    // Ipv4Address gwAddress("10.0.0.1");
    // Ptr<Ipv4StaticRouting> gateway = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (hub.Get (0)->Get
    // Object<Ipv4> ());
    // gateway->SetDefaultRoute (gwAddress, 1);



     // Create the lora channel object
    Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
    loss->SetPathLossExponent (3.76);
    loss->SetReference (1, 7.7);
    Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();
    Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

    // Create the LoraPhyHelper (Phy, Mac and Lora)
    LoraPhyHelper phyHelper = LoraPhyHelper ();
    phyHelper.SetChannel (channel);
    LorawanMacHelper macHelper = LorawanMacHelper ();
    LoraHelper helper = LoraHelper ();

    phyHelper.SetDeviceType(LoraPhyHelper::ED);
    macHelper.SetDeviceType(LorawanMacHelper::ED_A);
    NetDeviceContainer  endDevicesNetDevices = helper.Install(phyHelper, macHelper, nodes);

    phyHelper.SetDeviceType (LoraPhyHelper::GW);
    macHelper.SetDeviceType (LorawanMacHelper::GW);
    NetDeviceContainer endDeviceshub = helper.Install (phyHelper, macHelper, hub);
    macHelper.SetSpreadingFactorsUp (nodes, hub, channel);

    NodeContainer ghostNode;
    ghostNode.Create(1);
    ghostNode.Add(hub.Get(0));

    CsmaHelper csma;
    csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
    csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

    NetDeviceContainer hubDevice = csma.Install(ghostNode);

    //NetDeviceContainer p2pDevices;
    //p2pDevices = pointToPoint.Install(ghostNode);

    InternetStackHelper stack;
    stack.Install (ghostNode);
    Ipv4AddressHelper address;
    address.SetBase ("10.0.0.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pInterfaces;
    p2pInterfaces = address.Assign(hubDevice);


    Ipv4InterfaceContainer interfaces = address.Assign(hubDevice);
    tapBridge.Install(hub.Get(0),hubDevice.Get(0));
    //tapBridge.Install(hub.Get(0), endDeviceshub.Get(0));

    PeriodicSenderHelper periodicSenderHelper;
    periodicSenderHelper.SetPeriod (Seconds (12));
    periodicSenderHelper.Install (nodes);
    ForwardercdmaHelper ForwardercdmaHelper;
    ForwardercdmaHelper.Install (hub);

    printposition(hub);
    printposition(nodes);


    Simulator::Stop (Seconds(60.0));
    //NS_LOG_INFO ("Run Simulation.");

    Simulator::Run ();
    Simulator::Destroy ();
    //NS_LOG_INFO ("Done.");
    return 0;
}


MobilityHelper createscenario(double radio)
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "rho", DoubleValue (radio),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0),
                                 "Z", DoubleValue (0.0));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}

void printposition(NodeContainer n){// iterate our nodes and print their position.
    for (NodeContainer::Iterator j = n.Begin ();
           j != n.End (); ++j)
        {
        Ptr<Node> object = *j;
        Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
        NS_ASSERT (position != 0);
        Vector pos = position->GetPosition ();
        std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
    }
}
CommandLine setupcmd(){
    CommandLine cmd;
    cmd.AddValue ("radio", "Radio of the disc where random put the nodes", radio);
    cmd.AddValue ("numnodes", "Num. nodes in the grid for simulating",numnodes);
    return cmd;
}

MobilityHelper createstarcenter()
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (1.0),
                                 "DeltaY", DoubleValue (1.0),
                                 "GridWidth", UintegerValue (1),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}
