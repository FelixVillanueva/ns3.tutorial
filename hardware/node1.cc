
#include <iostream>
#include "ns3/core-module.h"
#include "ns3/network-module.h"


using namespace ns3;

int main (int argc, char *argv[]){
   
    Node n(0);
    std::cout<<"El nodo creado tiene el identificador "<< n.GetId()<<std::endl;
    
    Simulator::Stop (Hours (24));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


