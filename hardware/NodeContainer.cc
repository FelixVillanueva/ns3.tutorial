
#include <iostream>
#include <string>

#include "ns3/core-module.h"
#include "ns3/network-module.h"

#include "ns3/ptr.h"



using namespace ns3;
using namespace std;

void imprimirContainerIds(NodeContainer a, string ttag)
{
for(NodeContainer::Iterator i = a.Begin (); i != a.End (); i++)
    {
        Ptr<Node> o = *i;
        uint32_t id = o->GetId();
        std::cout <<ttag<<"Node Id "<<id<< std::endl;
    } 
}

int main (int argc, char *argv[]){
   
    Node n(0);
    Ptr<Node> n_ptr(&n);

    NodeContainer a(n_ptr);   
    
    imprimirContainerIds(a, "Contenedor a: ");
    
    NodeContainer b;
    b.Create(2);
    imprimirContainerIds(b, "Contenedor b: ");

    // NodeContainer c(a, b);
    NodeContainer c(a,b);
    imprimirContainerIds(c, "Contenedor c: ");


    Simulator::Stop (Hours (24));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


