#include "ns3/gnuplot.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/packet-socket-helper.h"
#include "ns3/packet-socket-address.h"

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include "ns3/applications-module.h"
#include "ns3/internet-module.h"


#include <algorithm>
#include <ctime>


using namespace ns3;

// you can modify this variables in argument command line e.g --radio=5.0
double radio = 20.0;
int numsensors = 1;

MobilityHelper createscenario(double radio); //random position around starcenter 
MobilityHelper createstarcenter();
void printposition(NodeContainer n);
CommandLine setupcmd();

int main (int argc, char *argv[]){
   
    

    CommandLine cmd =setupcmd();
    cmd.Parse (argc, argv);

    //LogComponentEnable ("UdpClient", LOG_LEVEL_ALL);
    //LogComponentEnable ("UdpServer", LOG_LEVEL_ALL);

    NodeContainer nodes;
    nodes.Create(numsensors);
    MobilityHelper scenario = createscenario(radio);
    scenario.Install (nodes);
    //put the gateway in 0.0 0.0 0.0  (center)
    NodeContainer gateway;
    gateway.Create(1);
    MobilityHelper gatewayposition = createstarcenter();
    gatewayposition.Install (gateway);

    WifiHelper wifi;
    YansWifiPhyHelper wifiPhy;// = YansWifiPhyHelper::Default ();
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
    WifiMacHelper wifiMac;
    wifiMac.SetType ("ns3::AdhocWifiMac");
    wifiPhy.SetChannel(wifiChannel.Create());
    NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);
    NetDeviceContainer gatewaydevice = wifi.Install (wifiPhy, wifiMac, gateway);
    
    // install internet protocols
    InternetStackHelper internet;
    internet.Install (nodes);
    internet.Install (gateway);
    Ipv4AddressHelper ipv4;
    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer i = ipv4.Assign (devices);
    Ipv4InterfaceContainer igateway= ipv4.Assign (gatewaydevice);
    
    //install apps clients
    Address gatewayAddress = Address(igateway.GetAddress (0));
    uint32_t packetSize = 32;
    uint16_t port = 9;
    UdpClientHelper client (gatewayAddress, port);
    Time interPacketInterval = Seconds (1.);
    client.SetAttribute("PacketSize", UintegerValue(packetSize));
    client.SetAttribute ("Interval", TimeValue (interPacketInterval));
    client.SetAttribute ("MaxPackets", UintegerValue (10));
    ApplicationContainer apps = client.Install(nodes);
    apps.Start(Seconds(2.0));
    apps.Stop(Seconds(10.0));
    
    // install apps server
    UdpServerHelper gatewayserver(port);
    ApplicationContainer gatewayapps = gatewayserver.Install(gateway.Get(0));
    gatewayapps.Start(Seconds(1.0));
    gatewayapps.Stop(Seconds(11.0)); 

    printposition(gateway);
    printposition(nodes);
  
    Simulator::Stop (Hours (24));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


MobilityHelper createscenario(double radio)
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "rho", DoubleValue (radio),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0),
                                 "Z", DoubleValue (0.0));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}

void printposition(NodeContainer n){// iterate our nodes and print their position.
    for (NodeContainer::Iterator j = n.Begin ();
           j != n.End (); ++j)
        {
        Ptr<Node> object = *j;
        Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
        NS_ASSERT (position != 0);
        Vector pos = position->GetPosition ();
        std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
    }
}
CommandLine setupcmd(){
    CommandLine cmd;
    cmd.AddValue ("radio", "Radio of the disc where random put the nodes", radio);
    cmd.AddValue ("numsensors", "Num. nodes in the grid for simulating",numsensors);
    return cmd;
}

MobilityHelper createstarcenter()
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (1.0),
                                 "DeltaY", DoubleValue (1.0),
                                 "GridWidth", UintegerValue (1),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}