#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

using namespace ns3;

int main (int argc, char *argv[]){
   
    double distanceX = 5.0;
    double distanceY = 5.0;
    int numnodes = 20;
    int nodesgridwidth = 5;

    CommandLine cmd;
    cmd.AddValue ("distanceX", "Distance X apart to place nodes (in meters).", distanceX);
    cmd.AddValue ("distanceY", "Distance Y apart to place nodes (in meters).", distanceY);
    cmd.AddValue ("numnodes", "Num. nodes in the grid for simulating",numnodes);
    cmd.AddValue ("nodesgridwidth", "Num. nododes in the row of a grid", nodesgridwidth);
    cmd.Parse (argc, argv);
   

    NodeContainer nodes;
    nodes.Create(numnodes);

    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (distanceX),
                                 "DeltaY", DoubleValue (distanceY),
                                 "GridWidth", UintegerValue (nodesgridwidth),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (nodes);

  // iterate our nodes and print their position.
    for (NodeContainer::Iterator j = nodes.Begin ();
           j != nodes.End (); ++j)
        {
        Ptr<Node> object = *j;
        Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
        NS_ASSERT (position != 0);
        Vector pos = position->GetPosition ();
        std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
    }
    Simulator::Destroy ();
    return 0;
}