#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

using namespace ns3;

// this variables can be modified by using arguments e.g --distanceX=20.0
double distanceX = 5.0;
double distanceY = 5.0;
int numnodes = 20;
int nodesgridwidth = 5;

MobilityHelper createscenario(double distanceX,double distanceY,int nodesgridwidth);
void printposition(NodeContainer n);
CommandLine setupcmd();

int main (int argc, char *argv[]){
   
    CommandLine cmd =setupcmd();
    cmd.Parse (argc, argv);
    
    NodeContainer nodes;
    nodes.Create(numnodes);
    MobilityHelper scenario = createscenario(distanceX,distanceY, nodesgridwidth);
    scenario.Install (nodes);
   
    printposition(nodes);
    Simulator::Destroy ();
    return 0;
}


MobilityHelper createscenario(double distanceX,double distanceY,int nodesgridwidth)
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (distanceX),
                                 "DeltaY", DoubleValue (distanceY),
                                 "GridWidth", UintegerValue (nodesgridwidth),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}

void printposition(NodeContainer n){// iterate our nodes and print their position.
    for (NodeContainer::Iterator j = n.Begin ();
           j != n.End (); ++j)
        {
        Ptr<Node> object = *j;
        Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
        NS_ASSERT (position != 0);
        Vector pos = position->GetPosition ();
        std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
    }
}
CommandLine setupcmd(){
    CommandLine cmd;
    cmd.AddValue ("distanceX", "Distance X apart to place nodes (in meters).", distanceX);
    cmd.AddValue ("distanceY", "Distance Y apart to place nodes (in meters).", distanceY);
    cmd.AddValue ("numnodes", "Num. nodes in the grid for simulating",numnodes);
    cmd.AddValue ("nodesgridwidth", "Num. nododes in the row of a grid", nodesgridwidth);
    return cmd;
}