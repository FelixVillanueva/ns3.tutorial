#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/class-a-end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/node-container.h"
#include "ns3/position-allocator.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/lora-radio-energy-model-helper.h"
#include "ns3/file-helper.h"
#include "ns3/names.h"

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include <algorithm>
#include <ctime>


using namespace ns3;
using namespace lorawan;

// this variables can be modified in the command line e.g --radio=20.0
double radio = 20.0;
int numnodes = 20;

MobilityHelper createscenario(double radio); //random position around starcenter 
MobilityHelper createstarcenter();
void printposition(NodeContainer n);
CommandLine setupcmd();

int main (int argc, char *argv[]){
   
    

    CommandLine cmd =setupcmd(numnodes, radio);
    cmd.Parse (argc, argv);

    LogComponentEnable ("LoraFrameHeader", LOG_LEVEL_ALL);
    //LogComponentEnable ("LorawanMacHeader", LOG_LEVEL_ALL);
    //LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_ALL);

    NodeContainer nodes;
    nodes.Create(numnodes);
    MobilityHelper scenario = createscenario(radio);
    scenario.Install (nodes);
    //put the hub in 0.0 0.0 0.0  center
    NodeContainer hub;
    hub.Create(1);
    MobilityHelper hubposition = createstarcenter();
    hubposition.Install (hub);

     // Create the lora channel object
    Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
    loss->SetPathLossExponent (3.76);
    loss->SetReference (1, 7.7);
    Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();
    Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

    // Create the LoraPhyHelper (Phy, Mac and Lora)
    LoraPhyHelper phyHelper = LoraPhyHelper ();
    phyHelper.SetChannel (channel);
    LorawanMacHelper macHelper = LorawanMacHelper ();
    LoraHelper helper = LoraHelper ();

    phyHelper.SetDeviceType(LoraPhyHelper::ED);
    macHelper.SetDeviceType(LorawanMacHelper::ED_A);
    NetDeviceContainer  endDevicesNetDevices = helper.Install(phyHelper, macHelper, nodes);
 
    phyHelper.SetDeviceType (LoraPhyHelper::GW);
    macHelper.SetDeviceType (LorawanMacHelper::GW);
    helper.Install (phyHelper, macHelper, hub);
    macHelper.SetSpreadingFactorsUp (nodes, hub, channel);
   
    PeriodicSenderHelper periodicSenderHelper;
    periodicSenderHelper.SetPeriod (Seconds (5));
    periodicSenderHelper.Install (nodes);

    printposition(hub);
    printposition(nodes);
  
    Simulator::Stop (Hours (24));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


MobilityHelper createscenario(double radio)
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "rho", DoubleValue (radio),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0),
                                 "Z", DoubleValue (0.0));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}

void printposition(NodeContainer n){// iterate our nodes and print their position.
    for (NodeContainer::Iterator j = n.Begin ();
           j != n.End (); ++j)
        {
        Ptr<Node> object = *j;
        Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
        NS_ASSERT (position != 0);
        Vector pos = position->GetPosition ();
        std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
    }
}
CommandLine setupcmd(){
    CommandLine cmd;
    cmd.AddValue ("radio", "Radio of the disc where random put the nodes", radio);
    cmd.AddValue ("numnodes", "Num. nodes in the grid for simulating",numnodes);
    return cmd;
}

MobilityHelper createstarcenter()
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (1.0),
                                 "DeltaY", DoubleValue (1.0),
                                 "GridWidth", UintegerValue (1),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}