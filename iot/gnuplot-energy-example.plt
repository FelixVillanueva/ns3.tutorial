set terminal png
set output "gnuplot-energy-example.png"
set title "Energy vs. Time \n\nTrace Source Path: /Names/EnergySource/RemainingEnergy"
set xlabel "Time (Seconds)"
set ylabel "Energy(J)"

set key inside
set datafile missing "-nan"
plot "gnuplot-energy-example.dat" index 0 title "EnergyRemaining" with linespoints
