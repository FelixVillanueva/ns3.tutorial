#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/packet-socket-helper.h"
#include "ns3/packet-socket-address.h"

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"

#include "ns3/applications-module.h"
#include "ns3/internet-module.h"


#include <algorithm>
#include <ctime>
#include <iostream>

using namespace ns3;

int numsensors = 3;
double radio = 8.0;

MobilityHelper createscenario(double radio); //random position around starcenter 
MobilityHelper createstarcenter();


int main (int argc, char *argv[]){
   
    
    NodeContainer nodes;
    nodes.Create(numsensors);
    MobilityHelper scenario = createscenario(radio);
    scenario.Install (nodes);
    //put the gateway in 0.0 0.0 0.0  (center)
    MobilityHelper gatewayposition = createstarcenter();
    gatewayposition.Install (nodes.Get(0));

    WifiHelper wifi;
    YansWifiPhyHelper wifiPhy;// = YansWifiPhyHelper::Default();
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
    wifiPhy.SetChannel(wifiChannel.Create ());

    WifiMacHelper wifiMac;
    wifiMac.SetType ("ns3::AdhocWifiMac");
    wifiPhy.SetChannel(wifiChannel.Create());
    NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);
    
    
    // install internet protocols
    InternetStackHelper internet;
    internet.Install (nodes);
    
    Ipv4AddressHelper ipv4;
    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer i = ipv4.Assign (devices);
    //Ipv4InterfaceContainer igateway= ipv4.Assign (gatewaydevice);
   
    // install apps server
    uint16_t port = 2000;
    UdpServerHelper server(port);
    //ApplicationContainer gatewayapps = server.Install(gateway.Get(0));
    ApplicationContainer gatewayapps = server.Install(nodes.Get(0));
    gatewayapps.Start(Seconds(1.0));
    gatewayapps.Stop(Seconds(11.0)); 
   
    //install apps clients
    //Address gatewayAddress = Address(igateway.GetAddress (0));
    Address gatewayAddress = Address(i.GetAddress (0));
    uint32_t packetSize = 32;
    uint32_t maxPacketCount = 10;
    UdpClientHelper client (gatewayAddress, port);
    Time interPacketInterval = Seconds (0.5);
    client.SetAttribute("PacketSize", UintegerValue(packetSize));
    client.SetAttribute ("Interval", TimeValue (interPacketInterval));
    client.SetAttribute ("MaxPackets",  UintegerValue(maxPacketCount));
    ApplicationContainer apps = client.Install(nodes);
    apps.Start(Seconds(2.0));
    apps.Stop(Seconds(10.0));
    
    
    //AsciiTraceHelper ascii;
    //wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("udp-echo.tr"));
    //wifiPhy.EnablePcapAll ("udp-echo", false);

    Simulator::Stop (Hours (24));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


MobilityHelper createscenario(double radio)
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "rho", DoubleValue (radio),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0),
                                 "Z", DoubleValue (0.0));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}

MobilityHelper createstarcenter()
{
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (1.0),
                                 "DeltaY", DoubleValue (1.0),
                                 "GridWidth", UintegerValue (1),
                                 "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    return mobility;
}